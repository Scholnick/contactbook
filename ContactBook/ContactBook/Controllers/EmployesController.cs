﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ContactBook.Models;
using ContactBook.Models.DAL;
using ContactBook.Repository;

namespace ContactBook.Controllers
{
    public class EmployesController : Controller
    {
        EmployesRepository _repEmployes;
        public EmployesController()
        {
            _repEmployes = new EmployesRepository();
        }
        public ActionResult Index()
        {
            return View(_repEmployes.getAllEmployees());
        }

        public ActionResult Details(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Employe employe = _repEmployes.getEmployes(id);
            if (employe == null)
            {
                return HttpNotFound();
            }
            return View(employe);
        }

        public ActionResult Create()
        {
            Employe employe = new Employe();
            employe.listCompany = _repEmployes.listCompanies();

            return View(employe);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Employe employe)
        {
            if (ModelState.IsValid)
            {
                _repEmployes.addEmploye(employe);
                return RedirectToAction("Index");
            }
            employe.listCompany = _repEmployes.listCompanies();
            return View(employe);
        }


        public ActionResult Edit(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employe employe = _repEmployes.getEmployes(id);
            if (employe == null)
            {
                return HttpNotFound();
            }
            employe.listCompany = _repEmployes.listCompanies();
            return View(employe);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Employe employe)
        {
            if (ModelState.IsValid)
            {
                _repEmployes.editEmploye(employe);
                return RedirectToAction("Index");
            }
            employe.listCompany = _repEmployes.listCompanies();
            return View(employe);
        }

        public ActionResult Delete(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employe employe = _repEmployes.getEmployes(id);
            if (employe == null)
            {
                return HttpNotFound();
            }
            return View(employe);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Employe employe = _repEmployes.getEmployes(id);
            _repEmployes.deleteEmploye(employe);
            return RedirectToAction("Index");
        }

    }
}
