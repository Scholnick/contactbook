﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ContactBook.Models;
using ContactBook.Models.DAL;
using ContactBook.Repository;

namespace ContactBook.Controllers
{
    public class CompaniesController : Controller
    {
        CompanieRepository _repCompany;
        public CompaniesController()
        {
            _repCompany = new CompanieRepository();
        }
        public ActionResult Index()
        {
            return View(_repCompany.getAllCaompanies());
        }

        public ActionResult Details(int id)
        {
            Company company;
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            company = _repCompany.getCompany(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Company company)
        {
            if (ModelState.IsValid)
            {
                _repCompany.addCompany(company);
                return RedirectToAction("Index");
            }

            return View(company);
        }

        public ActionResult Edit(int id)
        {
            Company company;
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            company = _repCompany.getCompany(id);

            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Company company)
        {
            if (ModelState.IsValid)
            {
                _repCompany.editCompany(company);
                return RedirectToAction("Index");
            }
            return View(company);
        }
        public ActionResult Delete(int id)
        {
            Company company;
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            company = _repCompany.getCompany(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }
                
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Company company;
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            company = _repCompany.getCompany(id);
            _repCompany.deleteCompany(company);
            return RedirectToAction("Index");
        }


    }
}
