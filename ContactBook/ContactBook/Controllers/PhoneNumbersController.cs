﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ContactBook.Models;
using ContactBook.Models.DAL;
using ContactBook.Repository;

namespace ContactBook.Controllers
{
    public class PhoneNumbersController : Controller
    {
        PhoneNumberRepository _repPhoneNumber;
        public PhoneNumbersController()
        {
            _repPhoneNumber = new PhoneNumberRepository();
        }

        public ActionResult Index()
        {
            return View(_repPhoneNumber.getAllPhonesWithEmploye());
        }

        public ActionResult Details(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PhoneNumber phoneNumber = _repPhoneNumber.getPhone(id);
            if (phoneNumber == null)
            {
                return HttpNotFound();
            }
            return View(phoneNumber);
        }

        public ActionResult Create()
        {
            PhoneNumber phoneNumber = new PhoneNumber();
            phoneNumber.listEmployees = _repPhoneNumber.listEmployees();
            return View(phoneNumber);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PhoneNumber phoneNumber)
        {
            if (ModelState.IsValid)
            {
                _repPhoneNumber.addPhone(phoneNumber);
                return RedirectToAction("Index");
            }
            phoneNumber.listEmployees = _repPhoneNumber.listEmployees();
            return View(phoneNumber);
        }

        public ActionResult Edit(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PhoneNumber phoneNumber = _repPhoneNumber.getPhone(id);
            if (phoneNumber == null)
            {
                return HttpNotFound();
            }
            phoneNumber.listEmployees = _repPhoneNumber.listEmployees();
            return View(phoneNumber);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PhoneNumber phoneNumber)
        {
            if (ModelState.IsValid)
            {
                _repPhoneNumber.editPhone(phoneNumber);
                return RedirectToAction("Index");
            }
            phoneNumber.listEmployees = _repPhoneNumber.listEmployees();
            return View(phoneNumber);
        }

        public ActionResult Delete(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PhoneNumber phoneNumber = _repPhoneNumber.getPhone(id);
            if (phoneNumber == null)
            {
                return HttpNotFound();
            }
            return View(phoneNumber);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PhoneNumber phoneNumber = _repPhoneNumber.getPhone(id);
            _repPhoneNumber.deletePhone(phoneNumber);
            return RedirectToAction("Index");
        }


    }
}
