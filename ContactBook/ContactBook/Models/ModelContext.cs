﻿using ContactBook.Models.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ContactBook.Models
{
    public class ModelContext : DbContext
    {
        public ModelContext() : base("DefaultConnection")
        {
        }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Employe> Employes { get; set; }
        public DbSet<PhoneNumber> PhoneNumber { get; set; }
    }
}