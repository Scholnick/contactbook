﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContactBook.Models.DAL
{
    public class PhoneNumber
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Номер")]
        [MaxLength(10, ErrorMessage = "Максимальная длина")]
        // [InputMask("(999) 999-9999")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Введений моб. телефон недійсний.")]
        public string Number { get; set; }

        [Display(Name = "Сотрудник")]
        public int EmployeId { get; set; }

        public IEnumerable<SelectListItem> listEmployees { get; set; }
        public virtual Employe Employe { get; set; }
    }
}