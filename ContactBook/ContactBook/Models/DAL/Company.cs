﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ContactBook.Models.DAL
{
    public class Company
    {

        [Key]
        public int ID { get; set; }

        [Required]
        [Display(Name = "Компания")]
        [MaxLength(20, ErrorMessage = "Максимальная длина")]
        public string Name { get; set; }

        public virtual ICollection<Employe> Employes { get; set; }
    }
}