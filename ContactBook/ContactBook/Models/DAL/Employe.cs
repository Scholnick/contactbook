﻿using ContactBook.Models.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContactBook.Models.DAL
{
    public class Employe
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [Display(Name ="Сотрудник")]
        [MaxLength(20, ErrorMessage = "Максимальная длина")]
        public string Name { get; set; }

        [Display(Name = "Компания")]
        public int CompanyId { get; set; }

        public IEnumerable<SelectListItem> listCompany { get; set; }
        public virtual Company Company { get; set; }
        public virtual ICollection<PhoneNumber> PhoneNumber { get; set; }

    }
}