﻿
using ContactBook.Models;
using ContactBook.Models.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ContactBook.Repository
{
    public class CompanieRepository
    {
        private ModelContext db = new ModelContext();
        public IEnumerable<Company> getAllCaompanies()
        {
            return db.Companies.ToList();
        }
        public Company getCompany(int id)
        {
            Company _company = db.Companies.Find(id);
            return _company;
        }
        public Company addCompany(Company company)
        {
            db.Companies.Add(company);
            db.SaveChanges();
            return company;
        }

        public Company editCompany(Company company)
        {
            db.Entry(company).State = EntityState.Modified;
            db.SaveChanges();
            return company;
        }
        public Company deleteCompany(Company company)
        {
            db.Companies.Remove(company);
            db.SaveChanges();
            return company;
        }
    }
}