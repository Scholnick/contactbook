﻿using ContactBook.Models;
using ContactBook.Models.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContactBook.Repository
{
    public class EmployesRepository
    {
        private ModelContext db = new ModelContext();
        public IEnumerable<Employe> getAllEmployees()
        {
            return db.Employes.ToList();
        }
        public Employe getEmployes(int id)
        {
            Employe _employe = db.Employes.Find(id);
            return _employe;
        }
        public Employe addEmploye(Employe employe)
        {
            db.Employes.Add(employe);
            db.SaveChanges();
            return employe;
        }

        public Employe editEmploye(Employe employe)
        {
            db.Entry(employe).State = EntityState.Modified;
            db.SaveChanges();
            return employe;
        }
        public Employe deleteEmploye(Employe employe)
        {
            db.Employes.Remove(employe);
            db.SaveChanges();
            return employe;
        }

        public IEnumerable<SelectListItem> listCompanies()
        {
            var _listCompanies = from r in db.Companies.ToList()
                                 select new SelectListItem
                                 {
                                     Text = r.Name,
                                     Value = r.ID.ToString()
                                 };

            return _listCompanies;
        }

    }
}