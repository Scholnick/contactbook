﻿using ContactBook.Models;
using ContactBook.Models.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContactBook.Repository
{
    public class PhoneNumberRepository
    {
        private ModelContext db = new ModelContext();
        public IEnumerable<PhoneNumber> getAllPhones()
        {
            return db.PhoneNumber.ToList();
        }
        public IEnumerable<PhoneNumber> getAllPhonesWithEmploye()
        {            
            return db.PhoneNumber.Include(p => p.Employe).ToList();
        }
        public PhoneNumber getPhone(int id)
        {
            PhoneNumber _phone = db.PhoneNumber.Find(id);
            return _phone;
        }
        public PhoneNumber addPhone(PhoneNumber phoneNumber)
        {
            db.PhoneNumber.Add(phoneNumber);
            db.SaveChanges();
            return phoneNumber;
        }

        public PhoneNumber editPhone(PhoneNumber phoneNumber)
        {
            db.Entry(phoneNumber).State = EntityState.Modified;
            db.SaveChanges();
            return phoneNumber;
        }
        public PhoneNumber deletePhone(PhoneNumber phoneNumber)
        {
            db.PhoneNumber.Remove(phoneNumber);
            db.SaveChanges();
            return phoneNumber;
        }

        public IEnumerable<SelectListItem> listEmployees()
        {
            var _listCompanies = from r in db.Employes.ToList()
                                 select new SelectListItem
                                 {
                                     Text = r.Name,
                                     Value = r.ID.ToString()
                                 };

            return _listCompanies;
        }

    }
}