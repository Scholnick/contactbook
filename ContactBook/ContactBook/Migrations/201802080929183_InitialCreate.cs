namespace ContactBook.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Employes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 20),
                        CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: true)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.PhoneNumbers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.String(nullable: false, maxLength: 10),
                        EmployeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employes", t => t.EmployeId, cascadeDelete: true)
                .Index(t => t.EmployeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PhoneNumbers", "EmployeId", "dbo.Employes");
            DropForeignKey("dbo.Employes", "CompanyId", "dbo.Companies");
            DropIndex("dbo.PhoneNumbers", new[] { "EmployeId" });
            DropIndex("dbo.Employes", new[] { "CompanyId" });
            DropTable("dbo.PhoneNumbers");
            DropTable("dbo.Employes");
            DropTable("dbo.Companies");
        }
    }
}
